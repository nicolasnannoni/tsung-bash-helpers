# This script is used to deploy Tsung on one or more machines
# Please refer to README for more info

# The only settings to change in this script is the TSUNG_VAR value
# It is the name of the .tar.gz file that should be downloaded from Tsung's website

# Tsung installation tar
TSUNG_TAR=tsung-1.5.1.tar.gz

echo ">> This script will install/update Tsung (latest in brew for Mac, $TSUNG_TAR for CentOS) and set up the nodes in nodes.csv"
echo ">> The first node in the list will be the master (used as controller, and that will generate reports)"
echo ">> ONLY TESTED WITH CENTOS/RH 7 and Mac OS X 10.9.4/10.10.0"
echo ">> "
echo ">> INFO: a public/private key pair must be generated with default names on all the nodes, and the machine running this script must have its own public key in the authorized_keys file of all the nodes (public key of master copied automatically to node)!"
echo ">> CAUTION: will ERASE hosts and limits.conf files on all the nodes!"
echo ">> CAUTION: will ERASE user's ~/.ssh/config file for the master node!"
echo ">> CAUTION: the user used to connect via SSH must be in the sudoers file of all the machines! (without password prompt: 'USER ALL=(ALL) NOPASSWD: ALL')"

set -e

while true; do
    read -p ">>>> Do you wish to continue?" yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

# Determine script location
SCRIPT_PATH="$( cd "$( dirname "$0" )" && pwd )"

# Preparing temporary folder
echo ">>>> Creating empty tmp folder"
cd "$SCRIPT_PATH"
rm -Rf tmp
mkdir -p tmp

# Build the hosts file
echo ">>>> Building the hosts file and setting the hostnames of the machines"
cd "$SCRIPT_PATH"

# Adding an endline character if needed (for proper read parse of the last line)
ENDHOSTS=$(tail -1 conf/nodes.csv | wc -l)
if [ "$ENDHOSTS" -eq "0" ]; then
    echo >> conf/nodes.csv
fi
NUMMACHINES=$(echo `wc -l < conf/nodes.csv`)

INDEX=-1
cp defaults/defaults-hosts tmp/hosts
touch tmp/loc-nodes.txt
touch tmp/ssh-config.txt
while IFS=, read -r system dest <&9;
do 
  INDEX=$((INDEX+1))
  if [ "$system" = "MAC" ] ; then
     IP=`ssh -t -t $dest sudo hostname node-$INDEX; ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -1`
  elif [ "$system" = "CENTOS" ] ; then
     IP=`ssh -t -t $dest "sudo hostname node-$INDEX && hostname -I"`
  else
     continue
  fi

  IP=`echo "$IP" | tr -d '\r'`
  printf "%s node-%s\n" "$IP" "$INDEX"  >> tmp/hosts 
  printf "node-%s\n" "$INDEX"  >> tmp/loc-nodes.txt 

  USERNAME=$(echo $dest | cut -f1 -d"@")
  printf "Host node-%s\n" "$INDEX"  >> tmp/ssh-config.txt 
  printf "User %s\n\n" "$USERNAME"  >> tmp/ssh-config.txt 
done 9< conf/nodes.csv

# Copy the master publickey to the server
echo ">>>> Getting the RSA public key of the master"
while IFS=, read -r system dest <&9;
do 
  scp $dest:~/.ssh/id_rsa.pub ./tmp
  break
done 9< conf/nodes.csv

echo ">>>> Downloading Tsung"
curl -o tmp/tsung.tar.gz http://tsung.erlang-projects.org/dist/$TSUNG_TAR

# Copy the Tsung installation script, hosts file and RSA of master to all nodes
echo ">>>> Copying installation script, Tsung, RSA and hosts file to all the nodes"
while IFS=, read -r system dest <&9;
do  
  if [ "$system" = "MAC" ] || [ "$system" = "CENTOS" ]; then
    rsync -rav --delete "$SCRIPT_PATH/" $dest:/tmp/tsung_deployment/
  else
    echo "Ignored (unrecognized) system ($system) for destination $dest."
  fi
done 9< conf/nodes.csv

# Removing the downloaded/generated files
echo ">>>> Cleanup"
cd "$SCRIPT_PATH"
rm -Rf tmp

# Launching the installation script on all the nodes
echo ">>>> Launching the installation script on all the nodes"
MACHINESINSTALLED=0
while IFS=, read -r system dest <&9;
do 
  if [ "$system" = "MAC" ] ; then
     echo ">> Starting installation on Mac, $dest"
     ssh -t -t $dest "source /etc/profile; sh /tmp/tsung_deployment/remote-scripts/install-tsung-osx.sh -auto"
     MACHINESINSTALLED=$((MACHINESINSTALLED+1))
  elif [ "$system" = "CENTOS" ] ; then
     echo ">> Starting installation on CentOS 7, $dest"
     ssh -t -t $dest sh /tmp/tsung_deployment/remote-scripts/install-tsung-centos7.sh -auto
     MACHINESINSTALLED=$((MACHINESINSTALLED+1))
  fi
done 9< conf/nodes.csv

echo ">> END! Tsung installed on $MACHINESINSTALLED/$NUMMACHINES machines!"