## Installation script executed on CentOS/RH 7 machines
## Do not run it manually: some files would be missing

###########
# Use set-up-tsung-nodes.sh instead!!! 
###########

set -e
echo ">> This script will install Tsung"
echo ">> ONLY COMPATIBLE WITH CENTOS/RH 7"

if [ "$1" == "-auto" ]; then
  echo "Automatic: no prompt mode"
else
	while true; do
	    read -p ">>>> Do you wish to continue?" yn
	    case $yn in
	        [Yy]* ) break;;
	        [Nn]* ) exit;;
	        * ) echo "Please answer yes or no.";;
	    esac
	done
fi

ISMASTER="no"

echo ">>>> Copying limits.conf file"
cd /tmp/tsung_deployment
USER=$(whoami)
sed -i -e "s/USERNAME/$USER/g" defaults/limits-centos.conf
sudo cp -f defaults/limits-centos.conf /etc/security/limits.conf

echo ">>>> Copying hosts file"
cd /tmp/tsung_deployment
sudo cp tmp/hosts /etc/hosts

echo ">>>> Adding public key of master to authorized key (if needed)"
key=`head -n 1 tmp/id_rsa.pub | cut -c1-80`
keyLoc=`head -n 1 ~/.ssh/id_rsa.pub | cut -c1-80`
if [[ -n $(cat ~/.ssh/authorized_keys | grep "$key") ]]; then
    echo ">> The key was already there"
else
    sudo cat tmp/id_rsa.pub >> ~/.ssh/authorized_keys
fi

if [ "$key" == "$keyLoc" ]; then
    echo ">> This is the master server, connect to all the nodes"
    ISMASTER="yes"
    for dest in $(<tmp/loc-nodes.txt ); do
	  IP=`ssh -t -oStrictHostKeyChecking=no ${dest} "exit"`
	done
fi

echo ">>>> Activating the latest EPEL"
sudo yum -y install epel-release
echo ">>>> Installing/updating Erlang and the required Tsung dependencies"
sudo yum -y install erlang perl gnuplot perl-Template-Toolkit
echo ">>>> Installing/updating Tsung"
cd tmp
tar zxfv tsung.tar.gz 
cd tsung-*
./configure
make
sudo make install

if [ "$ISMASTER" = "yes" ] ; then 
	echo ">> This is the master server, install master-specific stuff"
	cd /tmp/tsung_deployment
	
	echo ">>>> Replacing ~/.ssh/config and setting permissions"
	cp tmp/ssh-config.txt ~/.ssh/config
	sudo chmod 600 ~/.ssh/config

	echo ">>>> Adding a tsung-stats function for the current user"
	echo "function tsungstats()" > ~/.tsung-stats-function
	echo "{" >> ~/.tsung-stats-function
	echo "/usr/lib/tsung/bin/tsung_stats.pl"  >> ~/.tsung-stats-function
	echo "}" >> ~/.tsung-stats-function
	chmod u+x ~/.tsung-stats-function
fi

echo ">>>> Cleanup"
rm -Rf /tmp/tsung_deployment
host=`hostname`
printf ">>>> Deployment DONE! - %s\n" "$host"