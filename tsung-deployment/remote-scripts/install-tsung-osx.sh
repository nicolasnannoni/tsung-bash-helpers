## Installation script executed on Mac OS X machines
## Do not run it manually: some files would be missing

###########
# Use set-up-tsung-nodes.sh instead!!! 
###########

set -e
echo ">> This script will install Tsung 1.5.1"
echo ">> TESTED ONLY WITH MAC OS X 10.9.4"
echo ">> Need to restart the machine afterwards!"

if [ "$1" == "-auto" ]; then
  echo "Automatic: no prompt mode"
else
	while true; do
	    read -p ">>>> Do you wish to continue?" yn
	    case $yn in
	        [Yy]* ) break;;
	        [Nn]* ) exit;;
	        * ) echo "Please answer yes or no.";;
	    esac
	done
fi

TSUNG_FRESH="no"
ISMASTER="no"

echo ">>>> Copying limits.conf file"
cd /tmp/tsung_deployment
sudo cp -f defaults/limits-mac.conf /etc/security/limits.conf

echo ">>>> Copying hosts file"
cd /tmp/tsung_deployment
sudo cp tmp/hosts /etc/hosts

echo ">>>> Adding public key of master to authorized key (if needed)"
key=`head -n 1 tmp/id_rsa.pub | cut -c1-80`
keyLoc=`head -n 1 ~/.ssh/id_rsa.pub | cut -c1-80`
if [[ -n $(cat ~/.ssh/authorized_keys | grep "$key") ]]; then
    echo ">> The key was already there"
else
    sudo cat tmp/id_rsa.pub >> ~/.ssh/authorized_keys
fi

if [ "$key" == "$keyLoc" ]; then
    echo ">> This is the master server, connect to all the nodes"
    ISMASTER="yes"
    for dest in $(<tmp/loc-nodes.txt ); do
	  IP=`ssh -t -oStrictHostKeyChecking=no ${dest} "exit"`
	done
fi

echo ">>>> Update sshd_config (PermitUserEnvironment)"
sudo sed 's/^PermitUserEnvironment.*/PermitUserEnvironment yes/g' /etc/sshd_config > tmp/sshd_config
sudo cp tmp/sshd_config /etc/sshd_config

echo ">>>> Set the PATH variable for the current user to the current value (for non-interactive sessions)"
touch ~/.ssh/environment
grep -v '^PATH=' ~/.ssh/environment > tmp/environment || true
echo "PATH=$PATH" >> tmp/environment
sudo cp tmp/environment ~/.ssh/environment

echo ">>>> Checking whether Homebrew is installed or not"
command -v brew >/dev/null 2>&1 || { 
	echo ">> brew command not found"
	echo ">>>> Downloading and installing Homebrew"
	ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
}
echo ">> brew should be installed"

echo ">>>> Checking whether Tsung is installed or not"
command -v tsung >/dev/null 2>&1 || { 
	echo ">> tsung command not found"
	echo ">>>> Downloading and installing Tsung"
	brew install tsung
	TSUNG_FRESH="yes"
}

TSUNG_VERSION=`tsung -v`
if [ "$TSUNG_FRESH" = "no" ] ; then 
	echo ">> tsung command found, installed version $TSUNG_VERSION. Checking for updates..."
	brew upgrade tsung || true
fi

echo ">> tsung ($TSUNG_VERSION) installed"

if [ "$ISMASTER" = "yes" ] ; then 
	echo ">> This is the master server, install master-specific stuff"
	cd /tmp/tsung_deployment

	echo ">>>> Replacing ~/.ssh/config and setting permissions"
	cp tmp/ssh-config.txt ~/.ssh/config
	sudo chmod 600 ~/.ssh/config

	echo ">>>> Installing/updating cpanminus for silent cpan install"
	sudo cpan install App::cpanminus
	echo ">>>> Installing/updating Template toolkit (perl, for report generation)"
	sudo cpanm -f -i Template

	echo ">>>> Adding a tsung-stats function for the current user"
	ROOT_STATS=$(cd /usr/local/Cellar/tsung/*/lib/tsung/bin/ && pwd)
	echo "function tsungstats()" > ~/.tsung-stats-function
	echo "{" >> ~/.tsung-stats-function
	echo "$ROOT_STATS/tsung_stats.pl"  >> ~/.tsung-stats-function
	echo "}" >> ~/.tsung-stats-function
	chmod u+x ~/.tsung-stats-function
fi

echo ">>>> Cleanup"
cd /tmp/tsung_deployment
rm -Rf /tmp/tsung_deployment
host=`hostname`
printf ">>>> Deployment DONE! - %s\n" "$host"