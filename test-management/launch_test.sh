## Script launched on the master node
## Should not be modified or used directly

###########
# Use start_script.sh instead!!! 
###########

set -e

cd /tmp/tsung_test/
echo ">>>> Launching test"
tsung -f "/tmp/tsung_test/$1" -l /tmp/tsung_test/Logs start
cd /tmp/tsung_test/Logs/
cd "$(\ls -1dt */ | head -n 1)"
echo ">>>> Generating test report"
. ~/.tsung-stats-function
tsungstats