# This script is used to copy the whole folder containing this script to a local/remote 
# master node with Tsung installed, which will be used to run the tests

# /!\ Make sure to set the MASTER variable pointing to the master Tsung node (SSH connection string)
# Examples: username@remote.master.company.com or localuser@localhost

# Variable to set
MASTER=username@remote.master.company.com

#####################
set -e

# Get XML script to launch
if [[ -z "$1" ]]; then
  echo "Usage: start_script.sh [relative-path-to-xml-scenario]"
  echo "NOTE! path is relative to start_script.sh!"
  echo "NOTE! you need to set the MASTER variable in this script to username@master-hostname!"
fi

# Determine script location
ORIGINAL_PATH=$(pwd)
SCRIPT_PATH="$( cd "$( dirname "$0" )" && pwd )"

# Creating the Logs folder
cd $SCRIPT_PATH
mkdir -p Logs

# Copy the script on the master node
echo ">>>> Copying the test scenario to the master node"
rsync -ra --delete ${SCRIPT_PATH}/ ${MASTER}:/tmp/tsung_test/

# Run the test on the master node
echo ">>>> Starting the test scenario on the master node"
ssh -t ${MASTER} "source /etc/profile; cd /tmp/tsung_test/ && sh launch_test.sh $1" || true

echo ">>>> Downloading test results"
rsync -ra --delete ${MASTER}:/tmp/tsung_test/Logs/ ${SCRIPT_PATH}/Logs/
echo ">>>> Opening the report"
cd ${SCRIPT_PATH}/Logs
open ${SCRIPT_PATH}/Logs/"$(\ls -1dt */ | head -n 1)"report.html

echo ">> Path to report: " ${SCRIPT_PATH}/Logs/"$(\ls -1dt */ | head -n 1)"
cd $ORIGINAL_PATH
